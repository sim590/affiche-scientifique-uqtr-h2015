#--------------------------------------------------------
# File: Makefile    Author(s): Simon Désaulniers
# Date: 2015-03-01
#--------------------------------------------------------

document=affiche
compiler=pdflatex
bibcompiler=bibtex

out_exts := out log aux toc vrb snm nav pdf dvi bbl blg

.PHONY: clean $(document).pdf
$(document).pdf: $(document).tex $(document).bbl
	$(compiler) $(document).tex
$(document).bbl: $(document).bib
	$(compiler) $(document).tex
	$(bibcompiler) $(document)
	$(compiler) $(document).tex
clean:
	rm -f $(foreach ext,$(out_exts),$(document).$(ext))
