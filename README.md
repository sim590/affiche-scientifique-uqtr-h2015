# affiche-scientifique-uqtr-H2015

Mon affiche scientifique que je présente au concours d'affiches scientifique de
l'Université du Québec à Trois-Rivières au courant de la session d'hiver 2015.

## Sujet

Le projet [polyenum][] sera le sujet de l'affiche. Il sera question de parler
de différents aspects du projet comme :

- les polyominos (théorie);
- la structure globale du projet;
- la licence GPLV3;
- l'algorithme d'énumération derrière *polyenum*;
- la complexité de l'algorithme;
- performances du programme.

[polyenum]: https://bitbucket.org/ablondin/polyenum
