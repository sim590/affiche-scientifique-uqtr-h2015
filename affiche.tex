%----------------------------------------------------------
% File: affiche.tex         Author(s): Simon Désaulniers
% Date: 2015-03-04
%----------------------------------------------------------
% Affiche scientifique pour le concours d'affiche
% scientifique de l'Université du Québec à Trois-Rivières.
%----------------------------------------------------------

\documentclass[final,french]{beamer}
\mode<presentation>{\usetheme{confposter}}

\usepackage{babel}
\usepackage[utf8x]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}

\usepackage[orientation=landscape,size=a0,scale=1]{beamerposter}
\geometry{%
    hmargin=0cm, % little modification of margins
}

\usepackage{colortbl}
\usepackage{tikz}
\usetikzlibrary{fit,calc,positioning,decorations.pathreplacing,matrix}

\title{%
    % petit artifice pour le titre.
    % La disposition dépend du thème confposter
    \parbox{.2\textwidth}{\ }%
    \parbox{.5\textwidth}{%
        % Titre
        \centering Polyenum en liberté ! -- %
        % Sous-titre
        {\huge \emph{Outil d'énumération de polyominos} }%
    }%
    \parbox{.2\textwidth}{\hfill \includegraphics[height=6cm]{images/uqtr}}
    %\parbox{.2\textwidth}{\tiny Outil d'énumération de polyominos}
}
\author{Simon Désaulniers}
\institute{Université du Québec à Trois-Rivières}
\date{} %no date

\begin{document}
\begin{frame}[t,fragile] % DOIT ÊTRE PLACÉ AU DÉBUT DE LA LIGNE À CAUSE DE [fragile]
    \begin{columns}[t]
        \begin{column}{.31\linewidth}
            \begin{block}{Qu'est-ce qu'un polyomino ?}
                Un {\bf polyomino} est un ensemble de cellules représentées par des carrées et
                connectées par les côtés.
            \end{block}

            \begin{exampleblock}{Exemple de polyominos}
                Un {\bfseries polyomino à quatre cellules} est appelé un \emph{tétromino}. Toute
                personne ayant donc déjà joué au populaire jeu \emph{Tetris} connait les
                polyominos à quatre cellules.
                \begin{figure}[h]
                    \centering
                    \includegraphics[height=7cm]{images/spd}
                    \hspace{2cm}
                    \includegraphics[height=10cm]{images/arbre}
                    \caption{Polyominos à 4 cellules (tétromino)}
                    \label{fig:pol_4_cells}
                \end{figure}
            \end{exampleblock}

            \begin{block}{L'intérêt}
                Les \emph{polyominos} font l'objet d'étude en mathématiques combinatoires. Il existe
                des problèmes dont la solution n'est toujours pas connue.  Dans le domaine de la
                \emph{combinatoire}, le problème principal est celui de l'\emph{énumération} de ces
                objets. En effet, il n'existe pas encore de moyen analytique permettant d'énumérer
                les polyominos quelconques de façon efficace.
            \end{block}

            \begin{block}{Classification des polyominos}
                La {\bfseries classification} des polyominos est faite dans le but de \emph{diminuer
                la grandeur du problème et, ainsi réduire la complexité}. Par exemple, on a les
                différentes classes :
                \begin{itemize}
                    \item \emph{Arbres} : sans cycle (p. ex :
                        \includegraphics[height=1em]{images/arbre.png}, 3 feuilles);
                    \item \emph{Serpents} : arbre à 2 feuilles (p. ex :
                        \includegraphics[height=1em]{images/serpent.png});
                    \item \emph{Serpents partiellement dirigés} : serpents faisant des pas
                        \emph{nord}, \emph{est} ou \emph{ouest} (p. ex :
                        \includegraphics[height=1em]{images/spd.png}).
                \end{itemize}

                \begin{figure}[H]
                    \centering
                    \begin{tikzpicture}[scale=1.4]
                        \coordinate (p) at (0,0);
                        \coordinate (a) at (1,1);
                        \coordinate (s) at (1.8,1.8);

                        \draw (p) circle [radius=5];
                        \node at ($(p) - (2.5,2.5)$) {Polyominos};
                        \draw (a) circle (3.5);
                        \node at ($(a) - (1.5,1.5)$) {Arbres};
                        \draw (s) circle (2);
                        \node at ($(s) - (0.5,0.5)$) {Serpents};
                    \end{tikzpicture}
                    \caption{Classifications des polyominos}
                    \label{fig:classes-polyominos}
                \end{figure}
                Comme le montre le graphe ci-haut, les classes énumérées plutôt sont incluses
                l'une dans l'autre, c'est-à-dire que les serpents forment une \emph{sous-classe}
                des arbres qui sont eux-mêmes une sous-classe des polyominos. Bien sûr, les
                serpents partiellement dirigés sont des serpents.
            \end{block}

            \begin{alertblock}{Diviser pour mieux régner}
                \large
                \emph{
                    La stratégie utilisée est donc de compter les serpents pour obtenir davantage
                    d'informations sur les arbres, pour ensuite les compter à leur tour et ainsi de
                    suite.
                }
            \end{alertblock}
        \end{column}

        \begin{column}{.31\linewidth}
            \begin{block}{Algorithme}
                % TODO : Décrire l'algorithme de Jensen
                Dans son papier \cite{jensen-animaux}, Jensen, traite d'objets mathématiques
                qu'on appelle des \emph{animaux}. Il s'avère que ces objets se comportent comme
                des polyomnios. On peut donc utiliser ce résultat afin d'écrire un programme qui
                générera les polyominos.\\[1em]

                Le {\bfseries principe} est de remplir un rectangle colonne par colonne en
                ajoutant soit une \emph{cellule vide} ou une \emph{cellule pleine}. De cette
                façon, on créé un « arbre de possibilités » dans lequel on fera la recherche des
                polyominos satisfaisant la configuration demandée.\\[1em]
                \begin{figure}[H]
                    \centering
                    \begin{tikzpicture}[node distance=7em, minimum size=2.8cm]
                        \node (dots) [minimum size=1cm] {$\dots$};
                        \node (n1) [circle, draw=black,below left of=dots] {\emph{vide}};
                        \node (n2) [circle,draw=black,below right of=dots] {\emph{plein}};
                        \node (n3) [node distance=5em,circle, draw=black,
                            below left of=n1,yshift=-1em] {\emph{vide}};
                        \node (n4) [node distance=5em,circle,draw=black,
                            below right of=n1,yshift=-1em] {\emph{plein}};
                        \node [node distance=5em,below of=n2] {$\dots$};
                        \node [node distance=5em,below right of=n3] {$\dots$};

                        \draw[->] (dots.south) -- ([yshift=+5pt]n1.north);
                        \draw[->] (dots.south) -- ([yshift=+5pt]n2.north);
                        \draw[->] (n1.south) -- ([yshift=+5pt]n3.north);
                        \draw[->] (n1.south) -- ([yshift=+5pt]n4.north);
                    \end{tikzpicture}
                    \caption{Arbre de possibilités}
                    \label{fig:name}
                \end{figure}

                À la frontière du polyomino en « construction », on garde une information par
                rapport à la connexité avec le reste du polyomino. Cette information est encodée
                de la façon suivante :
                $$
                \left\{
                \begin{array}{l}
                    0 \ \text{ : cellule vide};\\
                    1 \ \text{ : cellule pleine isolée};\\
                    2 \ \text{ : première cellule d'un groupe connecté};\\
                    3 \ \text{ : cellule intermédiaire d'un groupe connecté};\\
                    4 \ \text{ : dernière cellule d'un groupe connecté};
                \end{array}
                \right.
                $$

                Admettons le cas de la figure \ref{fig:construction-pol} où on désigne la
                colonne en « construction » par la lettre $d$. La construction du polyomino est
                arrêté à l'insertion de la 3$^e$ cellule à partir du haut à gauche.  En ajoutant
                une \emph{cellule pleine} supplémentaire, on obtiendrait donc le nombre $1$.
                \begin{figure}[H]
                    \centering
                    \begin{tikzpicture}[scale=1.4]
                        \coordinate (hautRectangle) at (0,5);
                        \coordinate (droiteRectangle) at (8,0);

                        \draw (0,0) grid ($(hautRectangle) + (droiteRectangle)$);
                        \fill[red] (0.02,0.02) rectangle (0.98,0.98);
                        \fill[red] (0.02,2.02) rectangle (0.98,2.98);
                        \fill[red] (0.02,3.02) rectangle (0.98,3.98);
                        \fill[red] (0.02,4.02) rectangle (0.98,4.98);
                        \fill[red] (1.02,0.02) rectangle (1.98,0.98);
                        \fill[red] (1.02,1.02) rectangle (1.98,1.98);
                        \fill[red] (1.02,2.02) rectangle (1.98,2.98);
                        
                        \node at (2.5,4.5) [draw=red, minimum size=1.85em]{};
                        \node at (2.5,3.5) {0};
                        \node at (2.5,2.5) {4};
                        \node at (2.5,1.5) {3};
                        \node at (2.5,0.5) {2};

                        \node at (2.5,6.5) {1};
                        \draw[line width=1mm,->] (2.5,6) to [ bend left] (2.5,4.5);
                        \draw[line width=1mm,->] (2.5,-1) to (2.5,-0.5);
                        \node at (2.5,-1.5) {$d$};
                    \end{tikzpicture}
                    \caption{Construction d'un polyomino}
                    \label{fig:construction-pol}
                \end{figure}

                À l'itération suivante, on aurait donc une cellule isolé, \emph{mais pouvant devenir
                connecté par la suite !}
                \begin{figure}[H]
                    \centering
                    \begin{tikzpicture}[scale=1.4]
                        \coordinate (hautRectangle) at (0,5);
                        \coordinate (droiteRectangle) at (8,0);

                        \draw (0,0) grid ($(hautRectangle) + (droiteRectangle)$);
                        \fill[red] (0.02,0.02) rectangle (0.98,0.98);
                        \fill[red] (0.02,2.02) rectangle (0.98,2.98);
                        \fill[red] (0.02,3.02) rectangle (0.98,3.98);
                        \fill[red] (0.02,4.02) rectangle (0.98,4.98);
                        \fill[red] (1.02,0.02) rectangle (1.98,0.98);
                        \fill[red] (1.02,1.02) rectangle (1.98,1.98);
                        \fill[red] (1.02,2.02) rectangle (1.98,2.98);
                        
                        \fill[red] (2.02,0.02) rectangle (2.98,0.98);
                        \fill[red] (2.02,1.02) rectangle (2.98,1.98);
                        \fill[red] (2.02,2.02) rectangle (2.98,2.98);
                        \fill[red] (2.02,4.02) rectangle (2.98,4.98);

                        \draw[line width=1mm,->] (3.5,-1) -- (3.5,-0.5);
                        \node at (3.5,-1.5) {$d$};
                    \end{tikzpicture}
                    \caption{Construction d'un polyomino (partie 2)}
                    \label{fig:construction-pol-2}
                \end{figure}
            \end{block}
        \end{column}
        \begin{column}{.31\linewidth}

            \begin{block}{Résultats}
                Différents résultats \cite{tree-snake-max} ont étés rendu possible grâce à
                l'implémentation de l'algorithme précédemment décrit. Entre autres, on a :

                \begin{minipage}{.5\textwidth}
                \begin{table}[H]
                    \centering
                    \input{tableaux/table_max_trees-count.tex}
                    \caption{Nombre d'arbres maximaux ($w$: largeur, $h$: hauteur du rectangle)}
                    \label{tab:arbres_max}
                \end{table}
                \end{minipage}
                \begin{minipage}{.5\textwidth}
                \begin{table}[H]
                    \hspace{2cm}
                    \centering
                    \input{tableaux/table_max_snakes-count.tex}
                    \caption{Nombre de serpents maximaux ($w$: largeur, $h$: hauteur du rectangle)}
                    \label{tab:arbres_max}
                \end{table}
                \end{minipage}
            \end{block}
            \begin{block}{Polyenum}
                Le programme {\bfseries polyenum} est l'{\oe}uvre d'une équipe composée de
                chercheurs et d'étudiants de l'UQTR, de l'UQAM et de l'UQAC. Ce programme est sous
                license libre (GPLv3).  Parmi les membres de cette équipe, deux programmeurs
                travaillent activement à la conception du programme. Il s'agit d'\emph{Alexandre
                Blondin Massé} et de \emph{Simon Désaulniers}.

                \bigskip

                Les langages de programmation utilisés sont \emph{Python} et une variante de
                celui-ci nommé \emph{Cython}. L'utilisation de cette variante permet d'accroître
                la vitesse d'exécution du programme.

                \bigskip

                Ce paquet a été publié et est disponible aux sources suivantes :
                \begin{itemize}
                    \item \url{http://pypi.python.org/pypi/polyenum} (paquet installable)
                    \item \url{http://bitbucket.com/ablondin/polyenum} (code source)
                \end{itemize}

                \begin{figure}[h]
                    \centering
                    \includegraphics[height=4em]{images/python.png}
                    \hspace{0.2cm}
                    \includegraphics[height=4em]{images/cython.png}
                    \caption{Logos Python, Cython.}
                    \label{fig:logo_python_cython}
                \end{figure}
            \end{block}
            \begin{block}{License GPLv3}
                Selon la {\bfseries Free Software Foundation} le logiciel libre permet les 4
                libertés suivantes :
                \begin{enumerate}
                    \item \emph{The freedom to run the program as you wish, for any purpose (freedom 0).}
                    \item \emph{The freedom to study how the program works, and change it so it does your
                        computing as you wish (freedom 1). Access to the source code is a
                        precondition for this.}
                    \item \emph{The freedom to redistribute copies so you can help your neighbor (freedom 2).}
                    \item \emph{The freedom to distribute copies of your modified versions to others
                        (freedom 3).  By doing this you can give the whole community a chance to
                        benefit from your changes. Access to the source code is a precondition for
                        this.}
                \end{enumerate}
            \end{block}
            \begin{block}{Bibliographie}
                \bibliographystyle{plain}
                \bibliography{affiche}
            \end{block}
        \end{column}
    \end{columns}
\end{frame} 
% ^ DOIT ÊTRE PLACÉ AU DÉBUT DE LA LIGNE ET NE PAS CONTENIR AUTRE CHOSE SUR LA MÊME LIGNE À CAUSE DE
% [fragile]
\end{document}
